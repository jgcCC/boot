package com.example.jgc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

@SpringBootTest
class JgcApplicationTests {
    @Test
    void contextLoads() {
        LocalDate of = LocalDate.of(2023, 5, 4);
        of=of.plusDays(1);
        System.out.println(of);
    }

}
