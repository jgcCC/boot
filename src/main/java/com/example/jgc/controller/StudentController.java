package com.example.jgc.controller;

import com.example.jgc.entity.JsonResult;
import com.example.jgc.entity.Student;
import com.example.jgc.mapper.StudentMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    StudentMapper studentMapper;
    @GetMapping
    public JsonResult query(Student student,Integer pageNo,Integer pageSize){
        System.out.println(student.toString());
        PageHelper.startPage(pageNo,pageSize);
        List<Student> list = studentMapper.query(student);
        PageInfo<Student> pageInfo = new PageInfo<>(list);
        return JsonResult.ok(pageInfo);
    }

    @GetMapping("/page")
    public JsonResult page(Integer pageNo,Integer pageSize){
        PageHelper.startPage(pageNo,pageSize);
        List<Student> list = studentMapper.query(new Student());
        PageInfo<Student> pageInfo = new PageInfo<>(list);
        System.out.println(pageInfo);

        return JsonResult.ok(pageInfo);
    }

    @PostMapping
    public JsonResult add(@RequestBody Student student){
        studentMapper.add(student);
        return JsonResult.ok(student);
    }

    @GetMapping("/{id}")
    public JsonResult queryById(@PathVariable("id") Integer id){
       Student integer = studentMapper.queryById(id);
        return JsonResult.ok(integer);
    }

    @PutMapping
    public JsonResult update(@RequestBody Student student){
        if(student.getId()==null){
            return JsonResult.error(400,"学生id为空");
        }else{
            Integer integer = studentMapper.update(student);
            return JsonResult.ok(integer);
        }
    }

    @DeleteMapping("/{id}")
    public JsonResult deleteById(@PathVariable("id") Integer id){
       Integer row = studentMapper.delete(id);
       return JsonResult.ok(row);
    }

    @DeleteMapping("/delete")
    public JsonResult deleteByIds(Integer[] ids){
       Integer row = studentMapper.deleteByIds(ids);
       return JsonResult.ok(row);
    }
}
