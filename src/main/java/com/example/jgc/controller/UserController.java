package com.example.jgc.controller;

import com.example.jgc.entity.JsonResult;
import com.example.jgc.entity.User;
import com.example.jgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/login")
    @ResponseBody
    public JsonResult login(User user, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest){
        if(Objects.isNull(user.getUsername())  || Objects.isNull(user.getPassword())){
            return JsonResult.error(400,"账号或者·密码错误");
        }
        String token = userService.login(user);
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute("token",token);
        return JsonResult.ok("success");

    }
    @GetMapping("/hello")
    @ResponseBody
    public String hello(){
        return "hello";
    }

}
