package com.example.jgc.service.Impl;

import com.example.jgc.common.constant.UserConstant;
import com.example.jgc.entity.User;
import com.example.jgc.exception.LoginException;
import com.example.jgc.mapper.UserMapper;
import com.example.jgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public String login(User user) {
        User login = userMapper.login(user);
        if(Objects.isNull(login)){
            throw new LoginException("用户不存在");
        }
        if(!user.getPassword().equals(login.getPassword())){
            throw new LoginException("用户账号或者密码错误");
        }
        String token = UUID.randomUUID().toString().replace("-", "");

        stringRedisTemplate.opsForValue().set(UserConstant.USER_LOGIN_PRE+user.getId()+"",token);
        return token;
    }
}
