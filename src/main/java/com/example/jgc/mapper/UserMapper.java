package com.example.jgc.mapper;

import com.example.jgc.entity.User;

public interface UserMapper {
    User login(User user);
}
