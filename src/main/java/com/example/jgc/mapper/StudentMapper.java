package com.example.jgc.mapper;

import com.example.jgc.entity.Student;
import java.util.List;


public interface StudentMapper {
    List<Student> query(Student student);
    Student add(Student student);
    Student queryById(Integer id);
    Integer update(Student student);
    Integer delete(Integer id);
    Integer deleteByIds(Integer[] ids);
}
