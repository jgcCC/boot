package com.example.jgc.entity;

import lombok.Data;
@Data
public class Student {
    private String userName;
    private Integer id;
    private Integer age;
}
