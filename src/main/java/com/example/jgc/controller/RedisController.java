package com.example.jgc.controller;

import com.alibaba.fastjson.JSON;
import com.example.jgc.entity.Brand;
import com.example.jgc.entity.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class RedisController {
    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @GetMapping("/redis")
    public JsonResult redisTest() {
        redisTemplate.opsForValue().set("jike", "jiangGuangChen", 1, TimeUnit.HOURS);
        Brand brand = new Brand();
        brand.setBrandName("华为");
        brand.setCompanyName("公司名称");
        brand.setDescription(111+"");
        brand.setId(0);
        String s = JSON.toJSONString(brand);
        redisTemplate.opsForValue().set("brand",s);
        return JsonResult.ok("ok");
    }
}
