package com.example.jgc.interceptor;

import com.example.jgc.common.constant.UserConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginUserInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute("token");
        String id = request.getParameter("id");
        System.out.printf("---hhh");
        if(StringUtils.isEmpty(token)){
            return false;
        }else{
            String s = stringRedisTemplate.opsForValue().get(UserConstant.USER_LOGIN_PRE + id);
            if(!s.equals(token)){
                return false;
            }
            return true;
        }
    }
}
