package com.example.jgc.mapper;

import com.example.jgc.entity.Brand;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface BrandMapper {
    List<Brand> selectList();
    List<Brand> selectBrandsWithCon(Brand brand);
    Integer insertBrand(Brand brand);
    List<Brand> selectBrandsWithSingleCon(Brand brand);
    Integer updateBrand(Brand brand);
    Integer deleteBrandById(Integer id);
    List<Brand> page(@Param("pageNo") Integer pageNo,@Param("pageSize") Integer pageSize);
}
