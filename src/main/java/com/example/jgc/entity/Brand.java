package com.example.jgc.entity;

import lombok.Data;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
public class Brand {
    // id 主键
    private Integer id;
    // 品牌名称
    private String brandName;
    // 企业名称
    private String companyName;
    // 排序字段
    private Integer ordered;
    // 描述信息
    private String description;
    // 状态：0：禁⽤ 1：启⽤
    private Integer status;
//省略 setter and getter。⾃⼰写时要补全这部分代码
}