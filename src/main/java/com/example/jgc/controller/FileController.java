package com.example.jgc.controller;

import com.example.jgc.entity.JsonResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Controller
//@RequestMapping("/file")
public class FileController {
    /**
     * 只允许上次jpg或png后缀的文件
     * @param file
     * @return
     */
    @PostMapping("/upFile")
    @ResponseBody
    public JsonResult upFile(@RequestBody MultipartFile file) throws IOException {
        if (file==null){
            return JsonResult.error(400,"上传文件不能为空");
        }else{
            String originalFilename = "D:/"+file.getOriginalFilename();
            String name=originalFilename.substring(originalFilename.lastIndexOf(".")+1);
            if(name.toUpperCase().equals("JPG") || name.toUpperCase().equals("PNG")){
                file.transferTo(new File(originalFilename));
                return JsonResult.ok("ok");
            }else{
                return   JsonResult.error(400,"上传文件格式错误");
            }
        }



    }
}
