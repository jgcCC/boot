package com.example.jgc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.concurrent.ThreadPoolExecutor;

@MapperScan("com.example.jgc.mapper")
@SpringBootApplication
public class JgcApplication {

    public static void main(String[] args) {
        SpringApplication.run(JgcApplication.class, args);

    }

}
