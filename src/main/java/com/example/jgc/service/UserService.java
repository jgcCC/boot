package com.example.jgc.service;

import com.example.jgc.entity.User;

public interface UserService {
    String login(User user);
}
