package com.example.jgc.controller;

import com.example.jgc.entity.Brand;
import com.example.jgc.entity.JsonResult;
import com.example.jgc.mapper.BrandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {
    @Autowired
    BrandMapper brandMapper;
    @GetMapping("/list")
    public List<Brand> list(){
        return brandMapper.selectList();
    }
    @GetMapping("/con")
    public List<Brand> getBrandsWithConditon(Brand brand){
        System.out.printf(brand.toString());
        return brandMapper.selectBrandsWithCon(brand);
    }
    @PostMapping("/add")
    public Integer addBrand(@RequestBody Brand brand){
        Integer integer = brandMapper.insertBrand(brand);
        System.out.println(brand.toString());

        return integer;
    }
    @PutMapping("/modify")
    public Integer modifyBrand(@RequestBody @Validated Brand brand){
        Integer integer = brandMapper.updateBrand(brand);
        System.out.println("modify---"+brand.toString());
        return integer;
    }
    @DeleteMapping("/remove/{id}")
    public Integer removeBrand(@PathVariable("id") Integer id){
        Integer integer = brandMapper.deleteBrandById(id);

        return integer;
    }
    @GetMapping("/page")
    public JsonResult page(@RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize){
       if(pageNo==null||pageSize==null){
           return JsonResult.error(400,"分页数据不能为空");
       }
        pageNo=(pageNo-1)*pageSize;
      List<Brand> brands = brandMapper.page(pageNo,pageSize);
      return JsonResult.ok(brands);
    }
}
