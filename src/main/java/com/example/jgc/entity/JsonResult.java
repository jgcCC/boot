package com.example.jgc.entity;

import lombok.Data;

@Data
public class JsonResult {
    private String msg;
    private Integer code;
    private Object obj;
    public static JsonResult ok(Object obj){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setMsg("msg");
        jsonResult.setCode(200);
        jsonResult.setObj(obj);
        return jsonResult;
    }
    public static JsonResult error(Integer code,String msg){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(code);
        jsonResult.setMsg(msg);
        return jsonResult;
    }
}
